﻿using Desarrollo.Inventario.WebAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ch2018.WebAdmin.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin

      


        public ActionResult Autenticar(LoginVM login)
        {
           string  usuario = System.Configuration.ConfigurationManager.AppSettings["usuario"];
            string clave= System.Configuration.ConfigurationManager.AppSettings["clave"];


            if (usuario == login.usuario && login.clave == clave)
            {

              System.Web.Security.FormsAuthentication.SetAuthCookie(login.usuario, false);
              return RedirectToAction("Index");

                
            }
            else
            {
                TempData["credenciales"] = "0";
               return  RedirectToAction("Index", "Home");
            }
        }


        [Authorize]
        public ActionResult Logout(LoginVM datos)
        {
            if (System.Web.HttpContext.Current.Request.IsAuthenticated)
            {
                System.Web.Security.FormsAuthentication.SignOut();
            }
            return RedirectToAction("Index","Home");
        }


        [Authorize]
        public ActionResult index()
        {


            return View();
        }

      


    }
}