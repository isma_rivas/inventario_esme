﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Desarrollo.Inventario.WebAdmin.Models;

namespace Ch2018.WebAdmin.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (TempData["credenciales"] != null)
            {
                ViewBag.invalid = TempData["credenciales"].ToString();
            }

            LoginVM objloging = new LoginVM();

            return View(objloging);
        }

       
    }
}